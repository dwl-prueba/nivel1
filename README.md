Requerimientos: 
Aplicación que detecte si una persona tiene diferencias genéticas basándose en su  secuencia de ADN. Para eso es necesario crear un programa con un método o función con  la siguiente firma: 
boolean hasMutation(String[] dna) 
Este método debe recibir como parámetro un arreglo de Strings que representan cada fila  de una matriz (de NxN) con la secuencia del ADN. Las letras de los Strings solo pueden ser:  (A,T,C,G), las cuales representan cada base nitrogenada del ADN. 

Sin mutación: 
-  A T G C G A\ 
-  C A G T G C\ 
-  T T A T T T\ 
-  A G A C G G\ 
-  G C G T C A\ 
-  T C A C T G 

Con mutación: 
-   A T G C G A\ 
-   C A G T G C\ 
-   T T A T G T\ 
-   A G A A G G\ 
-   C C C C T A\ 
-    T C A C T G 

Sabrás si existe una mutación si se encuentra más de una secuencia de cuatro letras  iguales, de forma oblicua, horizontal o vertical. 
Ejemplo llamada a método con mutación: 

String[] dna = {"ATGCGA","CAGTGC","TTATGT","AGAAGG","CCCCTA","TCACTG"};\
hasMutation(dna); // devuelve true 
