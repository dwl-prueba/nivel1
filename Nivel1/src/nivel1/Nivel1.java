/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nivel1;

/**
 *
 * @author yair
 */
public class Nivel1 {

    static String A = "AAAA", T = "TTTT", C = "CCCC", G = "GGGG";
    public static boolean busquedaMutacion(String[][] dnasMatriz, int numeroBusqueda, int conteoMutaciones) {
        String cadennaVertical;
        String cadenaDiagonalDerecha1 = "";
        String cadenaDiagonalDerecha2 = "";
        String cadenaDiagonalIzquierda1 = "";
        String cadenaDiagonalIzquierda2 = "";
        boolean mutacion;
        if (conteoMutaciones > 2) {
            return true;
        } else {
            mutacion = false;
        }
        int idIzquierda = dnasMatriz.length - 1;
        for (int i = 0; i < dnasMatriz.length; i++) {
            try {
                if (numeroBusqueda > 0) {
                    cadenaDiagonalDerecha1 += dnasMatriz[i][i - numeroBusqueda];
                }
                cadenaDiagonalDerecha2 += dnasMatriz[i - numeroBusqueda][i];
                cadenaDiagonalIzquierda1 += dnasMatriz[i][idIzquierda];
                cadenaDiagonalIzquierda2 += dnasMatriz[Math.abs(i - 5)][Math.abs(idIzquierda - 5)];
                --idIzquierda;
            } catch (Exception e) {
            }
            if (numeroBusqueda == 0) {
                cadennaVertical = "";
                for (int j = 0; j < dnasMatriz[i].length; j++) {
                    cadennaVertical += dnasMatriz[j][i];
                }
                conteoMutaciones = conteoMutacion(conteoMutaciones, cadennaVertical);
            }
        }
        conteoMutaciones = conteoMutacion(conteoMutaciones, cadenaDiagonalDerecha1);
        conteoMutaciones = conteoMutacion(conteoMutaciones, cadenaDiagonalDerecha2);
        conteoMutaciones = conteoMutacion(conteoMutaciones, cadenaDiagonalIzquierda1);
        conteoMutaciones = conteoMutacion(conteoMutaciones, cadenaDiagonalIzquierda2);
        numeroBusqueda++;
        if (numeroBusqueda < dnasMatriz.length) {
            return busquedaMutacion(dnasMatriz, numeroBusqueda, conteoMutaciones);
        } else {
            return mutacion;
        }
    }
    public static Integer conteoMutacion(Integer conteoMutacion, String dna) {
        if (dna.contains(A) || dna.contains(T) || dna.contains(G) || dna.contains(C)) {
            conteoMutacion++;
        }
        return conteoMutacion;
    }
    public static boolean hasMutation(String[] dnas) {
        int conteoMutacion = 0;
        String[][] dnasMatriz = new String[dnas.length][dnas[0].length() - 1];
        for (int i = 0; i < dnas.length; i++) {
            conteoMutacion = conteoMutacion(conteoMutacion, dnas[i]);
            dnasMatriz[i] = dnas[i].split("");
        }
        return busquedaMutacion(dnasMatriz, 0, conteoMutacion);
    }

    ; 
    public static void main(String[] args) {
        String[] dna = {"ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG"}; // true
        //String[] dna = {"ATGCGA", "CAGTGC", "TTATTT", "AGACGG", "GCGTCA", "TCACTG"}; // false
        System.out.println(hasMutation(dna));
    }
    
}
